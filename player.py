from os import path

import pygame.key
from pygame import mixer
from pygame.image import load
from pygame.transform import rotozoom
from pygame.sprite import Sprite


class Player(Sprite):

    def __init__(self, x_start, ground_level):
        super().__init__()
        self.x_start = x_start
        self.ground_level = ground_level
        self.gravity = 0
        self.jump_gravity = -20

        # self.stand_frame = rotozoom(load(path.join('graphics', 'player', 'player_stand.png')), 0, 2.5)
        walk_frame_1 = load(path.join('graphics', 'player', 'player_walk_1.png')).convert_alpha()
        walk_frame_2 = load(path.join('graphics', 'player', 'player_walk_2.png')).convert_alpha()
        self.jump_frame = load(path.join('graphics', 'player', 'jump.png')).convert_alpha()

        self.frames = [walk_frame_1, walk_frame_2]
        self.frame_index = 0

        self.jump_sound = mixer.Sound(path.join('audio', 'jump.mp3'))
        self.jump_sound.set_volume(0.25)

        self.image = self.frames[self.frame_index]
        self.rect = self.image.get_rect(midbottom=(self.x_start, self.ground_level))

    def animate(self):
        if self.rect.bottom == self.ground_level:
            self.frame_index += 0.1
            if self.frame_index >= len(self.frames):
                self.frame_index = 0
            self.image = self.frames[int(self.frame_index)]
        else:
            self.image = self.jump_frame

    def on_ground(self):
        return self.rect.bottom == self.ground_level

    def apply_gravity(self):
        self.gravity += 1
        self.rect.y += self.gravity

    def is_below_ground(self):
        return self.rect.bottom > self.ground_level

    def put_on_ground(self):
        self.rect.bottom = self.ground_level

    def do_jump(self):
        self.jump_sound.play()
        self.gravity = self.jump_gravity

    def reset(self):
        self.rect.bottomleft = (self.x_start, self.ground_level)

    def collect_player_inputs(self):
        keys_pressed = pygame.key.get_pressed()
        if keys_pressed[pygame.K_SPACE] and self.on_ground():
            self.do_jump()

    def update(self, game_active=True):
        self.animate()
        self.collect_player_inputs()
        self.apply_gravity()
        if self.is_below_ground():
            self.put_on_ground()
