```
 ____  _   _ _   _ _   _ ___ _   _  ____   __  __    _    _   _ 
|  _ \| | | | \ | | \ | |_ _| \ | |/ ___| |  \/  |  / \  | \ | |
| |_) | | | |  \| |  \| || ||  \| | |  _  | |\/| | / _ \ |  \| |
|  _ <| |_| | |\  | |\  || || |\  | |_| | | |  | |/ ___ \| |\  |
|_| \_\\___/|_| \_|_| \_|___|_| \_|\____| |_|  |_/_/   \_\_| \_|
```

![img.png](img.png)

PyGame inspired and guided by tutorial: https://www.youtube.com/watch?v=AY9MnQ4x3zk
