from os import path

from pygame.event import Event
import pygame.key
from pygame.image import load
from pygame.sprite import Sprite


class Obstacle(Sprite):

    def __init__(self, x_start, ground_level, obstacle_type: str, avoided_event):
        super().__init__()
        self.x_start = x_start
        self.ground_level = ground_level
        self.obstacle_type = obstacle_type
        self.gravity = 0
        self.jump_gravity = -20
        self.velocity = 5
        self.avoided_event = avoided_event

        if self.obstacle_type == "snail":
            snail_frame_1 = pygame.image.load(path.join('graphics', 'snail', 'snail1.png')).convert_alpha()
            snail_frame_2 = pygame.image.load(path.join('graphics', 'snail', 'snail2.png')).convert_alpha()
            self.frames = [snail_frame_1, snail_frame_2]
            y_pos = ground_level
            self.animation_speed = 0.1

        elif self.obstacle_type == "fly":
            fly_frame_1 = pygame.image.load(path.join('graphics', 'fly', 'fly1.png')).convert_alpha()
            fly_frame_2 = pygame.image.load(path.join('graphics', 'fly', 'fly2.png')).convert_alpha()
            self.frames = [fly_frame_1, fly_frame_2]
            y_pos = ground_level - 100
            self.animation_speed = 0.3

        else:
            raise Exception(f"Unknown obstacle type: {obstacle_type}")

        self.frame_index = 0

        self.image = self.frames[self.frame_index]
        self.rect = self.image.get_rect(midbottom=(self.x_start, y_pos))

    def animate(self):
        self.frame_index += self.animation_speed
        if self.frame_index >= len(self.frames):
            self.frame_index = 0
        self.image = self.frames[int(self.frame_index)]

    def off_screen(self):
        return self.rect.right <= 0

    def destroy_if_off_screen(self):
        if self.off_screen():
            pygame.event.post(Event(self.avoided_event))
            self.kill()

    def update(self):
        self.animate()
        self.rect.x -= self.velocity
        self.destroy_if_off_screen()

