import os
from sys import exit
from random import randint

import pygame

from player import Player
from obstacle import Obstacle


class Game:

    def __init__(self):
        pygame.init()
        pygame.display.set_caption('Running Man!!!')
        self.width, self.height = 800, 400
        self.window = pygame.display.set_mode((self.width, self.height))

        self.black = (0, 0, 0)
        self.white = (255, 255, 255)
        self.red = (255, 0, 0)
        self.dark_blue = (26, 53, 85)
        self.pale_blue = "#c0e8ec"

        self.font = pygame.font.Font(os.path.join('font', 'Pixeltype.ttf'), 50)

        self.sky_image = pygame.image.load(os.path.join('graphics', 'sky.png')).convert()

        self.ground_image = pygame.image.load(os.path.join('graphics', 'ground.png')).convert()
        self.ground_level = 300

        self.player = pygame.sprite.GroupSingle(Player(100, self.ground_level))

        self.obstacles = pygame.sprite.Group()
        self.obstacle_velocity = 5

        self.obstacles_avoided = 0
        self.start_time = 0
        self.running_time = 0

        self.obstacle_spawn_time = 1500

        self.obstacle_spawn_timer = pygame.USEREVENT + 1
        pygame.time.set_timer(self.obstacle_spawn_timer, self.obstacle_spawn_time)

        self.snail_animation_timer = pygame.USEREVENT + 2
        pygame.time.set_timer(self.snail_animation_timer, 1000)

        self.fly_animation_timer = pygame.USEREVENT + 3
        pygame.time.set_timer(self.fly_animation_timer, 200)

        self.snail_avoided_event = pygame.USEREVENT + 4
        self.fly_avoided_event = pygame.USEREVENT + 5

        self.bg_music = pygame.mixer.Sound(os.path.join('audio', 'music.wav'))
        self.bg_music.set_volume(0.17)

        self.fps = 60
        self.clock = pygame.time.Clock()
        self.game_active = True
        self.run = True

    def draw(self):
        self.window.fill(self.dark_blue)
        self.window.blit(self.sky_image, (0, 0))
        self.window.blit(self.ground_image, (0, self.ground_level))

        self.player.draw(self.window)
        self.obstacles.draw(self.window)

        self.display_score()

    def game_over(self):
        self.window.fill(self.dark_blue)

        dead_text_font = self.font.render("You Got Dead!!!", False, self.white)
        dead_text = dead_text_font.get_rect(midtop=(self.width/2, 20))

        score_text_font = self.font.render(f"You avoided {self.obstacles_avoided} obstacles", False, self.white)
        score_text = score_text_font.get_rect(midbottom=(self.width/2, self.height - 80))

        time_text_font = self.font.render(f"In {self.running_time} seconds", False, self.white)
        time_text = time_text_font.get_rect(midbottom=(self.width / 2, self.height - 40))

        try_again_font = self.font.render("Try again... hit <ENTER>", False, self.white)
        try_again_text = try_again_font.get_rect(midbottom=(self.width / 2, self.height - 5))

        self.window.blit(dead_text_font, dead_text)
        self.player.draw(self.window)
        self.window.blit(score_text_font, score_text)
        self.window.blit(time_text_font, time_text)
        self.window.blit(try_again_font, try_again_text)

    def increment_obstacles_avoided(self):
        self.obstacles_avoided += 1

    def reset_score(self):
        self.obstacles_avoided = 0
        self.start_time = pygame.time.get_ticks()

    def update_run_time(self):
        self.running_time = int((pygame.time.get_ticks() - self.start_time) / 1000)

    def display_score(self):
        score_text_font = self.font.render(f'Obstacles Avoided: {self.obstacles_avoided} / Time Running: {self.running_time}', False, self.black)
        score_text = score_text_font.get_rect(midtop=(self.width / 2, 20))
        pygame.draw.rect(self.window, self.pale_blue, score_text)
        pygame.draw.rect(self.window, self.pale_blue, score_text, 5)
        self.window.blit(score_text_font, score_text)

    def obstacle_collision(self):
        sprites_collided = pygame.sprite.spritecollide(self.player.sprite, self.obstacles, False)
        if sprites_collided:
            return True
        else:
            return False

    def spawn_obstacle(self):
        x_pos = self.width + randint(0, 200)
        if randint(0, 2):
            self.obstacles.add(Obstacle(x_pos, self.ground_level, 'snail', self.snail_avoided_event))
        else:
            self.obstacles.add(Obstacle(x_pos, self.ground_level, 'fly', self.fly_avoided_event))

    def play(self):
        self.bg_music.play(loops=-1)
        while self.run:

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    self.run = False

                if self.game_active:

                    if event.type == self.obstacle_spawn_timer:
                        self.spawn_obstacle()

                    if event.type == self.snail_avoided_event or event.type == self.fly_avoided_event:
                        self.increment_obstacles_avoided()

                else:
                    if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                        self.reset_score()
                        self.obstacles.empty()
                        print("Starting new game")
                        self.game_active = True

            if self.game_active:
                if self.obstacle_collision():
                    print("Game Over :(")
                    self.game_active = False

                self.player.update(game_active=self.game_active)

                self.update_run_time()
                self.obstacles.update()
                self.draw()

            else:
                self.game_over()

            pygame.display.update()
            self.clock.tick(self.fps)

        pygame.quit()
        exit()